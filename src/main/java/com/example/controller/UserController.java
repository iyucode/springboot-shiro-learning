package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author liyan
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/add")
    public String toAdd(){
        return "user/add";
    }

    @RequestMapping("/update")
    public String toUpdate(){
        return "user/update";
    }

}
