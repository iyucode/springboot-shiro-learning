package com.example.mapper;

import com.example.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author liyan
 */

@Mapper
@Repository
public interface UserMapper {
    /**
     * 通过用户名获取User数据
     * @param username
     * @return
     */
    User queryUserByName(String username);
}
